const { defineConfig } = require('@vue/cli-service');
const packageConfig = require('./package.json');

// Add config on webpack
module.exports = defineConfig({
  transpileDependencies: true,
  pages: {
    index: {
      // Params of webpack compilation
      entry: 'src/main.ts',
      template: 'public/index.html',
      filename: 'index.html',
      chunks: ['chunk-vendors', 'chunk-common', 'index'],
      // Set variable for index html page
      // call with <%= htmlWebpackPlugin.options.VARIABLE %>
      author: packageConfig.author,
      keywords: packageConfig.keywords,
      description: packageConfig.description,
      title: packageConfig.name,
    },
  },
  // Todo fix wraning i18n on dev
  // chainWebpack: (config) => {
  //   config.module
  //     .rule('i18n')
  //     .resourceQuery(/\.json$/)
  //     .type('javascript/auto')
  //     .use('i18n')
  //     .loader('@/i18n/locale')
  //     .end()
  //     .end();
  //   config.plugin('define').tap((args) => args
  //     .map((e, i) => {
  //       if (i === 0) {
  //         return {
  //           ...args[0],
  //           MY_API_URL: JSON.stringify(process.env.URL),
  //           // other stuff
  //           __INTLIFY_PROD_DEVTOOLS__: false,
  //         };
  //       }
  //       return e;
  //     }));
  // },
});
