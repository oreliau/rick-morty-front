# rick-morty-front

## project init by

Template made by vueCLi (all package selected except scss processor, beacause tailwind is used here):

```bash
vue create rick-morty-front
```

## Setup project

```bash
yarn install && yarn dev
or
npm install && npm run dev
```

## Dev Configuration

### Es Lint by air BNB

For good pratices and very stric

### Typescript

Very Usefull to make less mistakes

### Jest

I use TDD/BDD with jest

- TDD : test driving developement
  - always start with a test who render false
  - code the minimun to succeed the test
- BDD : behaviour driving developement
  - think test as end user not as developer

```bash
npm run test:unit tests/unit
or
yarn test:unit tests/unit
```

for updating snapshot add tad `--updateSnapshot`

```bash
npm run test:unit --updateSnapshot
or
yarn test:unit --updateSnapshot
```

### Cypress

Used for test End To End

- install on your local machine

```bash
yarn cypress install
```

- use it with the awesome dashboard

```bash
yarn test:e2e
```

### GitLab-ci

automatisation of eslint test / test unit / build on container register on gitlab

### Class

BEM 101 method to create classe

## I18n

I used english, french and blurp (funny language reference to morty who burp often)

### Blurp Rules

- word lenght > 4
- 75% chance of appearing
- replace two firsts letter by 'bl' and two last by 'rp' and replace other
  letter by 'u'
- change loading logo

## Noob SEO

- keyword on link
- meta description
- FAQ (people ask question to google we should get this questions)

## A11y

For accesibilyty i use all semantic recommande by [w3c](https://validator.w3.org/) and [aria w3c](https://www.w3.org/TR/wai-aria-1.1/)
To test it lightroom in google chrome

Thinking for [NVDA](https://www.nvda-fr.org/) OpenSource logiciel digital reader

## Assets

### Images

All images are royalty free from [CLEANPNG](https://www.cleanpng.com/):

- logo.png made by [sonaliben](https://www.cleanpng.com/users/@sonaliben.html)
- logo-blurp.png made by [Horseguard](https://www.cleanpng.com/users/@horseguard.html)
- logo-404.png made by [zizwara](https://www.cleanpng.com/users/@zizwara.html)
- background made by [tessai](https://wall.alphacoders.com/big.php?i=876591&lang=French)

## Todo:

- Set a cache for images from external url
- e2e
- dark mode
- FAQ (people ask question to google we should get this questions)

## tempalte inspiration

from https://dribbble.com/shots/17551172-Defrag-NFT-Landing-Page/attachments/12695496?mode=media

## Issues

loading image keep prev image from pagination, on home page.
