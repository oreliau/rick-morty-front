import CollectionCards from '@/components/CollectionCards.vue';
import SearchCards from '@/components/SearchCards.vue';
import { setupI18n } from '@/i18n';
import setupRouter from '@/router';
import { config, shallowMount } from '@vue/test-utils';
import store, { key } from '@/store';

config.global.plugins = [setupI18n(), setupRouter(setupI18n()), [store, key]];

describe('filter component', () => {
  it('should filter page on submit', () => {
    const wrapper = shallowMount(SearchCards);
    const spy = jest.spyOn(wrapper.vm, 'searchSubmit');
    wrapper.find('form').trigger('submit');
    expect(spy).toHaveBeenCalled();
  });
  it('should display error if empty inputs', async () => {
    const wrapper = shallowMount(SearchCards, {
      submitState: {
        error: false,
        name: null,
        status: [],
        gender: [],
      },
    });
    await wrapper.find('form').trigger('submit');
    expect(wrapper.vm.error).toBeTruthy();
    expect(wrapper.find('#searchCard__error').exists).toBeTruthy();
  });
  describe('filter feature', () => {
    it('should display 1 character by filtre after recieved all caracter', async () => {
      await store.dispatch('fetchCharacters');
      await store.dispatch('filterCharacter', { name: 'brad' });
      const wrapper = shallowMount(CollectionCards);

      expect(wrapper.findAll('.card').length).toBe(4);
    });
    it('should have a btn to reset fitler', async () => {
      const wrapper = shallowMount(SearchCards);
      expect(wrapper.findAll('.searchCard__submit').length).toBe(1);
    });
  });
});
