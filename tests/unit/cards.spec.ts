import setupRouter from '@/router';
import { setupI18n } from '@/i18n';
import CollectionCards from '@/components/CollectionCards.vue';
import HomeView from '@/views/HomeView.vue';
import { mount, shallowMount, config } from '@vue/test-utils';
import store, { key } from '@/store';

config.global.plugins = [setupI18n(), setupRouter(setupI18n()), [store, key]];

describe('should automaticly load character data on api', () => {
  it('should mount collection component', () => {
    const wrapper = shallowMount(HomeView);
    expect(wrapper.find('collection-cards-stub').exists()).toBeTruthy();
  });
  it('should display card on api call', async () => {
    await store.dispatch('fetchCharacters');

    const wrapper = mount(CollectionCards);

    expect(wrapper.findAll('.card').length).toBe(20);
    expect(wrapper.findAll('.card__status').length).toBe(20);
    expect(wrapper.findAll('.card__species').length).toBe(20);
    expect(wrapper.findAll('.card__type').length).toBe(20);
    expect(wrapper.findAll('.card__gender').length).toBe(20);
    expect(wrapper.findAll('.card__position').length).toBe(20);
  });
  it('sould go to character page on click Card', async () => {
    await store.dispatch('fetchCharacters');

    const wrapper = mount(CollectionCards);

    expect(wrapper.find('.card__page').attributes().href).toMatch(/\/character\/\d*\/\w*\/$/);
  });
});
