import store, { key } from '@/store';
import { setupI18n } from '@/i18n';
import CharacterView from '@/views/CharacterView.vue';
import CharacterInformation from '@/components/CharacterInformation.vue';
import { config, mount, shallowMount } from '@vue/test-utils';
import LoadingPage from '@/components/LoadingPage.vue';
import setupRouter from '@/router';
import character from './dataSet/character.json';

config.global.plugins = [setupI18n(), setupRouter(setupI18n()), [store, key]];
config.global.components = { LoadingPage, CharacterInformation };

describe('Character page', () => {
  it('should render CharacterView with loadingPage', () => {
    const wrapper = shallowMount(CharacterView);
    expect(wrapper.find('.btn-homePage').exists()).toBeTruthy();
    expect(wrapper.find('character-information-stub').exists()).toBeTruthy();
  });
  it('should have picture and information', async () => {
    await store.dispatch('fetchCharacters');
    const wrapper = mount(CharacterInformation, {
      props: {
        characterId: '1',
      },
    });
    const dataset = character[0];
    await wrapper.vm.$nextTick();

    expect(wrapper.find('h2').text()).toBe(dataset.name);
    expect(wrapper.find('.character__img').attributes().src).toBe(dataset.image);
    expect(wrapper.find('.character__status__info').text()).toBe(dataset.status);
    expect(wrapper.find('.character__species__info').text()).toBe(dataset.species);
    expect(wrapper.find('.character__type__info').text()).toBe(dataset.type);
    expect(wrapper.find('.character__gender__info').text()).toBe(dataset.gender);
  });
});
