import SearchCards from '@/components/SearchCards.vue';
import { setupI18n } from '@/i18n';
import { mount, config, shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import setupRouter from '@/router';
import store, { key } from '@/store';
import HomeView from '@/views/HomeView.vue';
import SelectLanguage from '@/components/SelectLanguage.vue';

config.global.plugins = [setupI18n(), setupRouter(setupI18n()), [store, key]];

let wrapper = mount(App, {
  components: {
    HomeView,
    SelectLanguage,
  },
});

afterEach(() => {
  wrapper = mount(App, {
    components: {
      HomeView,
      SelectLanguage,
    },
  });
});
/**
 * Todo:
 * loading
 */
describe('Home Page test', () => {
  describe('main structure website', () => {
    it('render correctly', () => {
      expect(wrapper.html()).toMatchSnapshot();
    });
    it('should display a nav, footer; h1 tag', () => {
      expect(wrapper.find('nav').exists()).toBeTruthy();
      expect(wrapper.find('footer').exists()).toBeTruthy();
    });
  });

  describe('Display home page', () => {
    it('should display home page by default', () => {
      expect(wrapper.vm.$route.name).toContain('HomeView');
    });
    it('should have a title in english language by default', () => {
      expect(wrapper.find('#languageSelector option:checked').attributes()).toHaveProperty('value', 'en');
    });
    it('should display cards content', () => {
      expect(wrapper.find('#cards').exists()).toBeTruthy();
    });
    it('should display search form', () => {
      const wrapperSearch = shallowMount(SearchCards);
      expect(wrapperSearch.find('.searchCard__input').exists()).toBeTruthy();
      expect(wrapperSearch.find('.searchCard__submit').exists()).toBeTruthy();
      expect(wrapperSearch.findAll('.searchCard__filter').length).toBe(9);
    });
    it('should display next btn and selec', () => {
      expect(wrapper.find('.pagination__next').exists()).toBeTruthy();
      expect(wrapper.find('.pagination__pages').exists()).toBeTruthy();
      expect(wrapper.find('.pagination__count').exists()).toBeTruthy();
      expect(wrapper.find('.pagination__pagesCount').exists()).toBeTruthy();
    });
  });
});
