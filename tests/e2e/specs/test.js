// https://docs.cypress.io/api/table-of-contents
import packageConfig from '../../../package.json';

describe('h1 test', () => {
  // it('check structure for SEO', () => {
  //   cy.visit(`${packageConfig.homepage}en/`);
  //   cy.contains('h1', 'Rick and Morty Collection');
  //   cy.get('meta[name="description"]').should('have.attr', 'content', packageConfig.description);
  //   cy.get('meta[name="keywords"]').should(
  //     'have.attr',
  //     'content',
  //     packageConfig.keywords.join(','),
  //   );
  //   cy.get('meta[name="author"]').should('have.attr', 'content', packageConfig.author);
  // });
  // it('sould display cards on home page', () => {
  //   cy.visit(`${packageConfig.homepage}en/`);
  //   cy.get('.card').eq(19);
  //   cy.get('.card').each(($card) => {
  //     $card.get('.card__name');
  //     $card.get('.card__page');
  //     $card.get('.card__img');
  //     $card.get('.card__status');
  //     $card.get('.card__type');
  //     $card.get('.card__species');
  //     $card.get('.card__gender');
  //     $card.get('.card__position');
  //     $card.get('.card__location');
  //     $card.get('button');
  //   });
  // });
  it('should display page character on click on card button and use button return', () => {
    cy.visit(`${packageConfig.homepage}en/`);
    for (let i = 0; i < 20; i += 1) {
      cy.visit(`${packageConfig.homepage}en/`);
      let nameOfCharacter = null;
      cy.get('.card')
        .eq(i)
        .then(($card) => {
          nameOfCharacter = $card.find('.card__name').text();
          console.log($card.find('button').text());
          cy.wrap($card).click();
          cy.url().should('include', 'character');
          cy.get('h2').should('have.text', nameOfCharacter);
          cy.get('.btn-homePage button').click();
          cy.url().should('equal', `${packageConfig.homepage}en/`);
        });
    }
  });
});
