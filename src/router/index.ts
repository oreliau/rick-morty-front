import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { I18n } from 'vue-i18n';
import { setI18nLanguage, loadLocaleMessages, SUPPORT_LOCALES } from '../i18n';
import HomeView from '../views/HomeView.vue';
import CharacterViewVue from '../views/CharacterView.vue';

type I18nType = I18n<{
  en: {
    website: {
      title: string;
    };
    navigations: {
      home: string;
    };
  };
  fr: {
    website: {
      title: string;
    };
    navigations: {
      home: string;
    };
  };
  blurp: {
    website: {
      title: string;
    };
    navigations: {
      home: string;
    };
  };
}, unknown, unknown, boolean>

export default function setupRouter(i18n: I18nType) {
  const { global } = i18n;
  const locale = (typeof global.locale !== 'string') ? global.locale.value : global.locale;

  // setup routes
  const routes: Array<RouteRecordRaw> = [
    {
      path: '/:locale/',
      name: 'HomeView',
      component: HomeView,
    },
    {
      path: '/character/:characterId(\\d+)/:locale/',
      name: 'CharacterView',
      component: CharacterViewVue,
      props: true,
    },
    {
      path: '/:pathMatch(.*)*',
      redirect: () => `/${locale}/`,
    },
  ];

  const router = createRouter({
    history: createWebHistory(),
    routes,
  });

  router.beforeEach((to) => {
    const paramsLocale = to.params.locale;
    /**
     * if local not reconize redirect
     */
    if (typeof paramsLocale !== 'string' || !SUPPORT_LOCALES.includes(paramsLocale)) {
      return `/${locale}/`;
    }

    /**
     * load json for current language
     */
    if (!global.availableLocales.includes(paramsLocale)) {
      try {
        loadLocaleMessages(global.setLocaleMessage, paramsLocale);
      } catch (e) {
        console.error(e);
      }
    }

    setI18nLanguage(paramsLocale);
    return true;
  });

  return router;
}
