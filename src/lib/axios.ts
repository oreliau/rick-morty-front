import axios, { AxiosAdapter } from 'axios';
import { throttleAdapterEnhancer, cacheAdapterEnhancer, retryAdapterEnhancer } from 'axios-extensions';

export interface ResponseAxios {
  status: number,
}

const axiosAdaptater = axios.defaults.adapter;

const enchancerCacheTrhottle = (adapter: AxiosAdapter | undefined) => adapter
  && throttleAdapterEnhancer(
    cacheAdapterEnhancer(adapter),
    { threshold: 2 * 1000 },
  );

export const htpp = axios.create({
  baseURL: '/',
  headers: { 'Cache-Control': 'no-cache', SameSite: 'none' },
});

export const httpCachePrevent = axios.create({
  baseURL: '/',
  headers: { 'Cache-Control': 'no-cache', SameSite: 'none' },
  // cache will stay 5 minutes and trhottle request
  adapter: enchancerCacheTrhottle(axiosAdaptater),
});

export const httpRetry = axios.create({
  baseURL: '/',
  headers: { 'Cache-Control': 'no-cache', SameSite: 'none' },
  // will retry 2 times
  adapter: axiosAdaptater && retryAdapterEnhancer(axiosAdaptater),
});

export const axiosRequestGet = <T>(url: string): Promise<{
  data: T,
  status: number
}> => httpCachePrevent
    .get(url)
    .then((response) => (response))
    .catch(({ response }) => (response));
