/**
 * function replace some words by blurp
 */
export default (sentences: string) => (localStorage.getItem('locale-i18n') === 'blurp' ? sentences.match(/\w+/gm)?.map((word) => {
  if (word.length > 4 && Math.random() > 0.25) {
    return word.split('').map((e, i) => {
      switch (i) {
        case 0:
          return 'b';
        case 1:
          return 'l';
        case 2:
          return 'u';
        case word.length - 1:
          return 'p';
        case word.length - 2:
          return 'r';
        default:
          return 'u';
      }
    }).join('');
  }
  return word;
}, '').join(' ').trim()
  : sentences);
