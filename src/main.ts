import { createApp } from 'vue';
import App from '@/App.vue';
import '@/registerServiceWorker';
import store, { key } from '@/store';
import setupRouter from '@/router';
import { setupI18n } from '@/i18n';
import './index.css';

// module for translate words
const i18n = setupI18n();
const router = setupRouter(i18n);
const app = createApp(App);

app.use(i18n);
app.use(store, key);
app.use(router);
app.mount('#app');
