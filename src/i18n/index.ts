import { nextTick } from 'vue';
import { createI18n, LocaleMessageDictionary, VueMessageType, useI18n } from 'vue-i18n';
import en from '@/i18n/locale/en.json';
import fr from '@/i18n/locale/fr.json';
import blurp from '@/i18n/locale/blurp.json';
import blurpTransalte from '@/lib/blurpTranslate';

export const SUPPORT_LOCALES = ['en', 'fr', 'blurp'];

const optionsDefault = {
  globalInjection: false,
  legacy: false,
  locale: localStorage.getItem('locale-i18n') ?? 'en',
  fallbackLocale: 'fr',
  messages: {
    en,
    fr,
    blurp,
  },
};

export function setI18nLanguage(localeToSet: string) {
  document.querySelector('html')?.setAttribute('lang', localeToSet);
}

export function setupI18n(options = optionsDefault) {
  const i18n = createI18n(options);
  setI18nLanguage(options.locale);
  return i18n;
}

export function translate(path: string) {
  const { t, locale } = useI18n({ useScope: 'global' });
  if (locale.value === 'blurp') return blurpTransalte(t(path));
  return t(path);
}

export async function loadLocaleMessages(
  setLocaleMessage: (local: string, json: LocaleMessageDictionary<VueMessageType>) => void,
  locale: string,
) {
  const messages = await import(
    /* webpackChunkName: "locale-[request]" */ `./locale/${locale}.json`
  );
  if (Object.keys(JSON.stringify(messages)).length === 0) throw new Error('no json files found');

  setLocaleMessage(locale, messages.default);

  return nextTick();
}
