import { InjectionKey } from 'vue';
import { createStore, useStore as baseUseStore, Store } from 'vuex';
import cardsModule, { saveToLocalStorage, StateCard } from './cardsModule';

interface CardModule {
  cards: StateCard
}

export const key: InjectionKey<Store<CardModule>> = Symbol('');

export default createStore({
  strict: true,
  plugins: [saveToLocalStorage],
  modules: {
    cards: cardsModule,
  },
});

export function useStore() {
  return baseUseStore(key);
}
