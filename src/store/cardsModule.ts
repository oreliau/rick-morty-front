import { ActionTree, MutationTree, Plugin } from 'vuex';
import { axiosRequestGet } from '@/lib/axios';

type IdCharacter = number;

export type Character = {
  id: IdCharacter,
  name: string,
  status: 'Alive' | 'Dead' | 'unknown',
  species: string,
  type: string,
  gender: 'Female' | 'Male' | 'Genderless' | 'unknown',
  origin: {
    name: string,
    url: string,
  },
  location: {
    name: string,
    url: string,
  },
  image: string,
  episode: string[],
  url: string,
  created: string,
}

type Info = {
  count: number,
  pages: number,
  next: string,
  prev: string,
}

type QueryCharacter = {
  info: Info,
  results: Character[]

};

export type QueryEpisode = {
  id: number,
  name: string,
  'air_date': string,
  episode: string,
  characters: string[],
}

export type ListCardState = { [id: IdCharacter]: Character }

export interface StateCard {
  list: ListCardState;
  pageInfo: {
    count: number | null,
    pages: number | null,
    next: string | null,
    prev: string | null,
    currentPage: number,
    idByPage: { [pageId: number]: number[], }
  };
  loading: boolean,
  error404: boolean,
  episodes: {
    loading: boolean,
    listIdByCharacter: { [characterId: string]: string[], },
    list: {
      [episodeId: number]: QueryEpisode,
    },
  },
  filter: {
    loading: boolean,
    active: boolean,
    listId: { [pageId: number]: number[], },
    name: string[],
    pageFilter: {
      currentFilterPage: number,
      filterCount: number | null,
      filterPages: number | null,
      filterNext: string | null,
      filterPrev: string | null,
    };
  };
}

type FilterType = {
  name?: string,
  status?: string,
  species?: string,
  type?: string,
  gender?: string,
  page?: string
}

const resourceUri = 'https://rickandmortyapi.com/api';
const filterInit = {
  currentFilterPage: 1,
  filterCount: null,
  filterPages: null,
  filterNext: null,
  filterPrev: null,
};
const initState: StateCard = {
  list: {},
  pageInfo: {
    count: null,
    pages: null,
    next: null,
    prev: null,
    currentPage: 1,
    idByPage: {},
  },
  episodes: {
    loading: false,
    listIdByCharacter: {},
    list: {},
  },
  loading: false,
  error404: false,
  filter: {
    loading: false,
    active: false,
    listId: {},
    name: [],
    pageFilter: filterInit,
  },
};

/**
 * Compare expire time persistor and store the
 * persistor of vuex state store from localStorage
 *
 * @returns {object}
 */
const initializeState = (): StateCard => {
  const local = localStorage.getItem('vuexDate');
  const persitor = localStorage.getItem('vuexPersistCards');
  if (local && parseInt(local, 10) < Date.now() && persitor) {
    return { ...JSON.parse(persitor), filter: initState.filter };
  }
  return initState;
};

const storeState: StateCard = initializeState();

/**
 * Format data to store it
 * @param {Object | Array}
 * @returns {Object}
 */
const reduceDataResult = (
  data: { results: Character[], info?: Info } | Character[],
): { [id: string]: Character } => {
  const payload = !Array.isArray(data) ? data.results : data;

  return payload.reduce(
    (previous, current) => Object.assign(previous, { [current.id]: current }),
    {},
  );
};

/**
 * Fetch query for character route
 * if status is an error (>=400) make 3 retry with interval by 200ms
 * if on succeed stop clear it
 *
 * @param {string | null}
 * @returns {void}
 */
const fetchRequest = async (query: string | null = null):
  Promise<[QueryCharacter | Character[], number]> => {
  let response: { data: QueryCharacter | Character[], status: number } = await axiosRequestGet(`${resourceUri}/character/${query}`);
  if (response.status >= 400) {
    let retry = 0;
    const timer = await setInterval(async () => {
      const responseRetry: { data: QueryCharacter | Character[], status: number } = await axiosRequestGet(`${resourceUri}/character/${query}`);
      if (responseRetry.status === 200 && responseRetry.data) {
        clearInterval(timer);
        response = responseRetry;
      }
      if (retry >= 1) {
        clearInterval(timer);
      }
      retry += 1;
    }, 200);
  }

  return [response.data, response.status];
};

const actions = <ActionTree<StateCard, unknown>>{
  /**
   * @this fetchEpisodes async
   * fetch all episodes for a user
   * and commit responses
   *
   * @param store
   * @param {{episodesUrl: Array, characterId: number}}
   * @returns {void}
   */
  async fetchEpisodes(store, { episodesUrl, characterId }:
    { episodesUrl: string[], characterId: number }) {
    store.commit('updateLoadingEpisode', true);
    const ids = episodesUrl.reduce((prev, url) => {
      const match = url.match(/\d+$/gm);
      const id = match && match[0];

      if (id && !(characterId in store.state.episodes.listIdByCharacter)) {
        return [...prev, id];
      }
      return prev;
    }, [] as string[]);

    if (ids.length) {
      const responses = await axiosRequestGet(`${resourceUri}/episode/${ids.join(',')}`);
      if (responses.status === 200) {
        store.commit('updateEpisodeList', responses.data);
        store.commit('updateidCharacterEpisode', { characterId, ids });
      }
    }
    store.commit('updateLoadingEpisode', false);
  },
  /**
   * @this fetchCharacters async
   * fetch a page from api to get character to display
   * if succeeds loops 10 other page silent commit of @this loopOtherPages
   *
   * @param store
   * @param {number}
   * @returns {void}
   */
  async fetchCharacters(store, payload = store.state.pageInfo.currentPage) {
    store.commit('updateLoadingStatus', true);
    store.commit('updateError404Status', false);
    const { state: { pageInfo: { idByPage } } } = store;

    if (idByPage[payload]) {
      store.commit('changePage', payload);
      store.commit('updateLoadingStatus', false);
    } else {
      const [data, status]: [QueryCharacter | Character[], number] = await fetchRequest(`?page=${payload}`);
      if (status === 200) {
        const list = reduceDataResult(data);
        store.commit('updateList', list);
        store.commit('updatePageInfo', data);
        store.commit('registerIdByPage', { data, payload });
        store.commit('changePage', payload);
        store.commit('updateLoadingStatus', false);

        await store.dispatch('loopOtherPages');
      }
    }
    store.commit('updateLoadingStatus', false);
  },
  /**
   * @this silentFetchCharacters async
   * fetch without commit loading
   *
   * @param store
   * @param {number} idPage
   * @returns {void}
   */
  async silentFetchCharacters(store, payload: number) {
    const page: number = payload;
    const [data, status]: [QueryCharacter | Character[], number] = await fetchRequest(`?page=${payload}`);
    if (status === 200) {
      const list = reduceDataResult(data);
      store.commit('updateList', list);
      store.commit('updatePageInfo', data);
      store.commit('registerIdByPage', { data, page });
    }
  },
  /**
   * @this loopOtherPages
   * Promise all query
   *
   * @param store
   * @returns {Object}
   */
  loopOtherPages(store) {
    const { state: { pageInfo: { pages, idByPage } } } = store;
    if (!pages) return null;

    const fetchs = [];
    let counter = 0;
    for (let i = 1; i <= pages; i += 1) {
      if (!idByPage[i]) {
        counter += 1;
        fetchs.push(store.dispatch('silentFetchCharacters', i));
      }
      if (counter === 10) break;
    }
    return Promise.all(fetchs);
  },
  /**
   * @this fetchCharacter async
   * fetch a character
   *
   * @param store
   * @param {number}
   * @returns {void}
   */
  async fetchCharacter(store, payload: number) {
    store.commit('updateLoadingStatus', true);
    store.commit('updateError404Status', false);

    const [data, status]: [QueryCharacter | Character[], number] = await fetchRequest(`[${payload}]`);

    if (Array.isArray(data)) {
      if (status === 200 && data.length > 0) {
        const list = reduceDataResult(data);
        store.commit('updateList', list);
        store.commit('updateLoadingStatus', false);
        store.dispatch('fetchEpisodes', { episodesUrl: data[0].episode, characterId: payload });
      } else {
        store.commit('updateLoadingStatus', false);
        store.commit('updateError404Status', true);
      }
    }
  },
  /**
   * @this filterCharacter async
   * fetch a character
   *
   * @param store
   * @param {Object}
   * @returns {void}
   */
  async filterCharacter(store, payload: FilterType) {
    store.commit('updateLoadingFilter', true);

    const { state: { filter: { pageFilter: { filterNext, filterPrev } } } } = store;
    const urlTemplate = filterNext || filterPrev;

    let query = null;
    if (payload.page && urlTemplate) {
      const split = urlTemplate.match(/[?]\b[a-zA-Z0-9=_&-.*+@%]+/);
      if (split) {
        query = split[0].replace(/page=\w+/, `page=${payload.page}`);
      }
    } else {
      query = Object.entries(payload).reduce(
        (prev, [key, value], index) => (value === 'none' || value === '' ? prev : `${index === 0 ? prev : `${prev}&`}${[key]}=${[value]}`),
        '?',
      );
    }
    const [data, status]: [QueryCharacter | Character[], number] = await fetchRequest(query);
    if (status === 200) {
      const list = reduceDataResult(data);
      store.commit('updateList', list);
      store.commit('filterCharacter', data);
      store.commit('updatePageInfoFilter', data);
      store.commit('registerIdByPageFilter', { data, page: payload.page ?? 1 });
      store.commit('updateCurrentPageFilter', payload.page ?? 1);
    } else if (status === 404) {
      store.commit('noResultFromFilter');
    }
    store.commit('updateLoadingFilter', false);
  },
  /**
   * @this resetFilter
   * reset filter store
   *
   * @param store
   * @returns {void}
   */
  resetFilter(store) {
    store.commit('initFilter');
  },
};

const mutations = <MutationTree<StateCard>>{
  // list
  updateList(state: StateCard, payload: ListCardState) {
    state.list = { ...state.list, ...payload };
  },
  registerIdByPage(state: StateCard, payload: { data: QueryCharacter, page: number }) {
    const ids = payload.data.results.map((e) => e.id);
    state.pageInfo.idByPage = {
      ...state.pageInfo.idByPage,
      [payload.page]: ids,
    };
  },
  changePage(state, payload: number) {
    state.pageInfo.currentPage = payload;
  },
  updatePageInfo(state: StateCard, payload: QueryCharacter) {
    state.pageInfo = {
      ...state.pageInfo,
      count: payload.info.count,
      pages: payload.info.pages,
      next: payload.info.next,
      prev: payload.info.prev,
    };
  },
  // filter
  updateLoadingFilter(state: StateCard, payload: boolean) {
    state.filter.loading = payload;
  },
  noResultFromFilter(state: StateCard) {
    state.filter = {
      ...state.filter,
      listId: {},
      name: [],
      pageFilter: {
        currentFilterPage: 1,
        filterCount: 0,
        filterPages: 0,
        filterNext: null,
        filterPrev: null,
      },
    };
  },
  filterCharacter(state: StateCard, payload: QueryCharacter) {
    const nameOnQuery = payload.results.map((e) => e.name);

    state.filter = {
      ...state.filter,
      active: true,
      name: nameOnQuery,
    };
  },
  registerIdByPageFilter(state: StateCard, payload: { data: QueryCharacter, page: number }) {
    const ids = payload.data.results.map((e) => e.id);
    state.filter.listId = {
      ...state.filter.listId,
      [payload.page]: ids,
    };
  },
  updatePageInfoFilter(state: StateCard, payload: QueryCharacter) {
    state.filter.pageFilter = {
      ...state.filter.pageFilter,
      filterCount: payload.info.count,
      filterPages: payload.info.pages,
      filterNext: payload.info.next,
      filterPrev: payload.info.prev,
    };
  },
  updateCurrentPageFilter(state: StateCard, payload: number) {
    state.filter.pageFilter.currentFilterPage = payload;
  },
  initFilter(state: StateCard) {
    state.filter = {
      ...state.filter,
      active: false,
      listId: [],
      name: [],
    };
  },
  // episodes
  updateEpisodeList(state: StateCard, payload: QueryEpisode | QueryEpisode[]) {
    const list = Array.isArray(payload)
      ? payload.reduce((prev, curr) => ({ ...prev, [curr.id]: curr }), {})
      : { [payload.id]: payload };
    state.episodes.list = {
      ...state.episodes.list,
      ...list,
    };
  },
  updateLoadingEpisode(state: StateCard, payload: boolean) {
    state.episodes.loading = payload;
  },
  updateidCharacterEpisode(state: StateCard, { characterId, ids }:
    { ids: string[], characterId: string }) {
    state.episodes.listIdByCharacter = Object.assign(
      state.episodes.listIdByCharacter,
      { [characterId]: ids },
    );
  },
  // page utility
  updateLoadingStatus(state: StateCard, payload: boolean) {
    state.loading = payload;
  },
  updateError404Status(state: StateCard, payload: boolean) {
    state.error404 = payload;
  },
};

const getters = {
  // List
  getList: (state: StateCard) => state.list,
  getNextPage: (state: StateCard) => state.pageInfo.next,
  getPreviousPage: (state: StateCard) => state.pageInfo.prev,
  getCountCharacter: (state: StateCard) => state.pageInfo.count,
  getCountPage: (state: StateCard) => state.pageInfo.pages,
  getCurrentPage: (state: StateCard) => state.pageInfo.currentPage,
  getCurrentPageList: (state: StateCard) => `${state.pageInfo.currentPage}` in state.pageInfo.idByPage
    && state.pageInfo.idByPage[state.pageInfo.currentPage]
      .map((id) => (state.list[id])),
  // Filter
  getNameFilter: (state: StateCard) => state.filter.name,
  getLoadingFilter: (state: StateCard) => state.filter.loading,
  getStatusFilter: (state: StateCard) => state.filter.active,
  getNextPageFilter: (state: StateCard) => state.filter.pageFilter.filterNext,
  getPreviousPageFilter: (state: StateCard) => state.filter.pageFilter.filterPrev,
  getCountCharacterFilter: (state: StateCard) => state.filter.pageFilter.filterCount,
  getCountPageFilter: (state: StateCard) => state.filter.pageFilter.filterPages,
  getCurrentPageFilter: (state: StateCard) => state.filter.pageFilter.currentFilterPage,
  getListFilter: (state: StateCard) => `${state.filter.pageFilter.currentFilterPage}` in state.filter.listId
    && state.filter.listId[state.filter.pageFilter.currentFilterPage]
      .map((id) => (state.list[id])),
  // episode
  getEpisodes: (state: StateCard) => state.episodes.list,
  getEpisodesById: (state: StateCard) => (id: string) => id in state.episodes.listIdByCharacter
    && state.episodes.listIdByCharacter[id].map((e) => state.episodes.list[parseInt(e, 10)]),
  getLoadingEpisodes: (state: StateCard) => state.episodes.loading,
  // page utilty
  getLoading: (state: StateCard) => state.loading,
  getError404: (state: StateCard) => state.error404,
};

const cardsModule = {
  state: () => (storeState),
  mutations,
  actions,
  getters,
};

// persist cards store
export const saveToLocalStorage: Plugin<{ cards: StateCard, lang: string }> = (store) => {
  store.subscribe((mutation, { cards }) => {
    if (!mutation.type.match(/filter/gmi)) {
      localStorage.setItem('vuexPersistCards', JSON.stringify({ ...cards }));
    }
    if (!localStorage.getItem('vuexDate')) {
      const expireTime = (24 * 60 * 60 * 60 * 1000) + Date.now();
      localStorage.setItem('vuexDate', expireTime.toString());
    }
  });
};

export default cardsModule;
