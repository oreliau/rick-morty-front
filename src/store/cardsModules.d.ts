declare module '*.vue' {
    export type IdCharacter = number;

    export type Character = {
        id: IdCharacter,
        name: string,
        status: 'Alive' | 'Dead' | 'unknown',
        species: string,
        type: string,
        gender: 'Female' | 'Male' | 'Genderless' | 'unknown',
        origin: {
            name: string,
            url: string,
        },
        location: {
            name: string,
            url: string,
        },
        image: string,
        episode: string[],
        url: string,
        created: string,
    }

    export type Info = {
        count: number,
        pages: number,
        next: string,
        prev: string,
    }

    export type QueryCharacter = {
        info: Info,
        results: Character[]

    };

    export type QueryEpisode = {
        id: number,
        name: string,
        'air_date': string,
        episode: string,
        characters: string[],
    }

    export type ListCardState = { [id: IdCharacter]: Character }

    export interface StateCard {
        list: ListCardState;
        pageInfo: {
            count: number | null,
            pages: number | null,
            next: string | null,
            prev: string | null,
            currentPage: number,
            idByPage: { [pageId: number]: number[], }
        };
        loading: boolean,
        error404: boolean,
        episodes: {
            loading: boolean,
            listIdByCharacter: { [characterId: string]: string[], },
            list: {
                [episodeId: number]: QueryEpisode,
            },
        },
        filter: {
            active: boolean,
            listId: { [pageId: number]: number[], },
            name: string[],
            pageFilter: {
                currentFilterPage: number,
                filterCount: number | null,
                filterPages: number | null,
                filterNext: string | null,
                filterPrev: string | null,
            };
        };
    }

    export type FilterType = {
        name?: string,
        status?: string,
        species?: string,
        type?: string,
        gender?: string,
        page?: string
    }

}
